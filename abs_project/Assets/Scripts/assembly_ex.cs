﻿//    Automation Box Simulator - assembly script 2
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;

public class assembly_ex : MonoBehaviour
{
    private GameObject selectedObject = null;
    public bool dist;
    Transform mounting_plate;
    public Camera cam_1;
    public AudioClip set_detail;

    [HideInInspector]
    public static int counter = 0;

    void Start ()
    {

	}

	void Update ()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 3))
        {
            if (hit.collider.tag == "Detail")
            {
                selectedObject = hit.collider.gameObject;
                dist = true;
                if (Input.GetKeyDown(KeyCode.Mouse0) && selectedObject.GetComponent<Renderer>().material.shader == Shader.Find("Unlit/XRay"))
                {
                    AudioSource.PlayClipAtPoint(set_detail, new Vector3(cam_1.transform.position.x, cam_1.transform.position.y, cam_1.transform.position.z));
                    if (selectedObject.transform.parent == true)
                    {
                        Transform[] kids;
                        kids = selectedObject.transform.parent.GetComponentsInChildren<Transform>();
                        foreach (Transform gob in kids)
                        {
                            gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Standard");
                            gob.GetComponentInChildren<Renderer>().material.color = Color.white;
                        }
                    }
                    else
                    {
                        selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                        selectedObject.GetComponent<Renderer>().material.color = Color.white;
                    }
                    if (assembly.a_step != 7)
                    {
                        mounting_plate = GameObject.Find("mounting_plate").transform;
                        if (hit.transform.parent == true)
                        {
                            hit.transform.parent.parent = mounting_plate;
                        }
                        else
                        {
                            selectedObject.transform.parent = mounting_plate;
                        }
                        counter++;
                    }
                    if(assembly.a_step == 7)
                    {
                        mounting_plate = GameObject.Find("box_door").transform;
                        if (hit.transform.parent == true)
                        {
                            hit.transform.parent.parent = mounting_plate;
                        }
                        else
                        {
                            selectedObject.transform.parent = mounting_plate;
                        }
                        counter++;
                    }
                    switch (assembly.a_step)
                    {
                        case 0:
                            if (counter == 5)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 1:
                            if (counter == 3)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 2:
                            if (counter == 3)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 3:
                            if (counter == 4)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 4:
                            if (counter == 6)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 5:
                            if (counter == 2)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 6:
                            if (counter == 5)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 7:
                            if(counter == 7)
                            {
                                assembly.a_step++;
                                counter = 0;
                            }
                            break;
                        case 9:

                            break;
                    }
                }
            }
        }
        
    }
}

﻿//    Automation Box Simulator - assembly script
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class assembly : MonoBehaviour
{
    private GameObject selectedObject = null;
    private bool colorDel = true;//проверка на снятый цвет
    public static bool isAssemblyRezh = false;
    public FirstPersonController pl;
    public Camera cam_1;
    public static int a_step = 0;
    static bool mp_setted = false;//установлена ли монтажная панель в шкаф
    public GameObject provodka;

    [HideInInspector]
    public bool dist;
    public Inv_item selectedItem;

    void OnGUI()
    {
        if (colorDel == false && a_step < 8)
        {
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 400, 30), "Нажмите 'Е' чтобы перейти в режим сборки");
        }
        if (isAssemblyRezh == true || doorScript.assemblyOnDoor == true)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 400, 30), "Нажмите 'F' чтобы выйти из режима сборки");
        }
        if (colorDel == false && a_step == 8 && mp_setted == false)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 400, 30), "Нажмите 'E' чтобы взять монтажную панель в руки");
        }
        if (colorDel == false && a_step == 8 && mp_setted == true)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 400, 30), "Нажмите 'E' чтобы установить монтажную панель");
        }
        if (colorDel == false && a_step == 9)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 400, 30), "Нажмите 'E' чтобы перейти в режим установки проводки");
        }
    }
	
	void Update ()
    {
        if (isAssemblyRezh == false)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit, 3) && isAssemblyRezh == false)
            {
                if (hit.collider.tag == "Assemb")
                {
                    if (selectedObject != null)
                    {
                        selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                        selectedObject = null;
                    }
                    selectedObject = hit.collider.gameObject;
                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Selection");//добавить выделение объекта
                    dist = true;
                    colorDel = false;
                    if (Input.GetButtonDown("Use"))
                    {
                        if (a_step < 8)//переход в режим сборки
                        {
                            selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                            colorDel = true;
                            isAssemblyRezh = true;
                            pl.transform.position = new Vector3(12.483f, 1.746f, 19.112f);
                            cam_1.transform.rotation = Quaternion.Euler(90, 0, 0);
                        }
                        if (a_step == 8 && mp_setted == true)//устанавливаем монтажную панель в шкаф
                        {
                            Transform[] obj_kids;//меняем шейдер
                            obj_kids = selectedObject.transform.GetComponentsInChildren<Transform>();
                            foreach (Transform gob in obj_kids)
                            {
                                gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Standard");
                                gob.GetComponentInChildren<Renderer>().material.color = Color.white;
                            }
                            a_step++;
                            mp_setted = false;
                        }
                        if (a_step == 8 && mp_setted == false)//берем предмет в руки, если монтажная панель уже собрана
                        {
                            Transform[] obj_kids;//меняем шейдер
                            obj_kids = selectedObject.transform.GetComponentsInChildren<Transform>();
                            foreach (Transform gob in obj_kids)
                            {
                                gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                            }
                            selectedObject.transform.position = new Vector3(7.637f, 1.757f, 16.583f);
                            selectedObject.transform.rotation = Quaternion.Euler(0, 90, 0);
                            mp_setted = true;
                        }
                        if(a_step == 9)//переходим в режим проводки
                        {
                            WireSetScript.wireSetRezh = true;
                            provodka.SetActive(true);
                        }
                    }
                }
                if (hit.collider.tag != "Assemb" && colorDel == false)
                {
                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                    selectedObject = null;
                    colorDel = true;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.F) && isAssemblyRezh == true)//выход из режима сборки
        {
            isAssemblyRezh = false;
            pl.enabled = true;
            pl.transform.position = new Vector3(12.069f, 1.1690f, 17.141f);
            cam_1.transform.rotation = Quaternion.Euler(17, 0, 0);
        }
        if (isAssemblyRezh == true || WireSetScript.wireSetRezh == true)
        {
            pl.enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}

﻿//    Automation Box Simulator - load script
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System;

[System.Serializable]
public class saves
{
    public string name;
    public string saveID;
}

public class LoadDataScript : MonoBehaviour
{
    //сохранения
    public GameObject sampleButton;
    public Transform contentPanel;
    public GameObject sampleButton2;
    public Transform contentPanel2;
    public SqliteConnection conn;
    public List<saves> saveList = new List<saves>();
    public List<saves> historyList = new List<saves>();
    public GameObject userSaves;
    public GameObject userHistory;
    public GameObject dialog_export;

    [HideInInspector]
    public static bool isLoadedSaves = false;
    public static bool isLoadedHistory = false;
    

    void Start()
    {
        string connectionString = "URI=file:abs_db.db";
        conn = new SqliteConnection(connectionString);
        conn.Open();
    }

    public void btn_Close_dialog_Click()
    {
        userSaves.SetActive(false);
    }

    public void btn_Close_dialog_history_Click()
    {
        userHistory.SetActive(false);
    }

    public void load_history()//загрузка списка истории пользователя
    {
        userHistory.SetActive(true);
        if (isLoadedHistory == false)
        {
            SqliteCommand cmd = new SqliteCommand("SELECT history_id, assessment, date FROM history WHERE user_id = @user_id", conn);
            cmd.Parameters.Add(new SqliteParameter("@user_id", SignScript.userID));
            SqliteDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                saves temp_history = new saves();
                temp_history.saveID = Convert.ToString(rd.GetInt32(0));
                temp_history.name = "Баллов: "+Convert.ToString(rd.GetInt32(1))+" Дата: "+Convert.ToString(rd.GetDateTime(2));
                historyList.Add(temp_history);
            }
            rd.Close();
            foreach (var saves in historyList)
            {
                if (saves.name != null && saves.name != "")
                {
                    GameObject newButton = Instantiate(sampleButton2) as GameObject;
                    HistoryScript historyScript = newButton.GetComponent<HistoryScript>();
                    historyScript.nameLabel.text = saves.name;
                    historyScript.saveID.text = saves.saveID;
                    newButton.transform.SetParent(contentPanel2);
                }
            }
            isLoadedHistory = true;
        }
    }

    public void OnExportClickComplete()
    {
        HistoryScript.isDialog = false;
        dialog_export.SetActive(false);//скрываем диалог
    }

    void Update()
    {
        if(HistoryScript.isDialog == true)
        {
            dialog_export.SetActive(true);
        }
    }

    public void load_Saves()//загрузка списка сборок пользователя
    {
        userSaves.SetActive(true);
        if (isLoadedSaves == false)
        {
            SqliteCommand cmd = new SqliteCommand("SELECT level_id, date FROM levels WHERE user_id = @user_id", conn);
            cmd.Parameters.Add(new SqliteParameter("@user_id", SignScript.userID));
            SqliteDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                saves temp_save = new saves();
                temp_save.saveID = Convert.ToString(rd.GetInt32(0));
                temp_save.name = Convert.ToString(rd.GetDateTime(1));
                saveList.Add(temp_save);
            }
            rd.Close();
            foreach (var saves in saveList)
            {
                if (saves.name != null && saves.name != "")
                {
                    GameObject newButton = Instantiate(sampleButton) as GameObject;
                    SavesScript saveScript = newButton.GetComponent<SavesScript>();
                    saveScript.nameLabel.text = saves.name;
                    saveScript.saveID.text = saves.saveID;
                    newButton.transform.SetParent(contentPanel);
                }
            }
            isLoadedSaves = true;
        }
    }
}

﻿//    Automation Box Simulator - door of box script
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class doorScript : MonoBehaviour
{
    private static bool isOpen = false;//проверка на открытую или закрытую дверцу шкафа
    private static bool isOpenF = false;
    private static bool trig_help = false;
    private GameObject selectedObject = null;
    private bool colorDel = true;//проверка на снятый цвет
    public Animator anim;
    public static bool assemblyOnDoor = false;
    public FirstPersonController pl;
    public Camera cam_1;
    public AudioClip door_open;
    public AudioClip door_close;

    [HideInInspector]
    public bool dist;
    public static bool isFirstOpened = false;//проверка, антибаг на проверку открытой двери в первый раз

    void Awake ()
    {
        anim = GetComponent<Animator>();
	}

    void OnGUI()
    {
        if (colorDel == false && assemblyOnDoor == false)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 250, 30), "Нажмите 'Е' чтобы взаимодействовать");
            if (assembly.a_step == 7 && isOpen == false && isFirstOpened == false)
            {
                GUI.color = Color.black;
                GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 80, 300, 30), "Нажмите 'R' чтобы перейти в режим сборки");
            }
            if (assembly.a_step == 7 && isOpenF == true && isFirstOpened == true)
            {
                GUI.color = Color.black;
                GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 80, 300, 30), "Нажмите 'R' чтобы перейти в режим сборки");
            }
        }
    }

	void Update ()
    {
        if (assembly.isAssemblyRezh == false && assemblyOnDoor == false)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit, 3))
            {
                if (hit.collider.tag == "BoxDoor")
                {
                    if (selectedObject != null)
                    {
                        selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                        selectedObject = null;
                    }
                    selectedObject = hit.collider.gameObject;
                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Selection");//добавить выделение объекта
                    dist = true;
                    colorDel = false;
                    if (Input.GetButtonDown("Use"))
                    {
                        if (isFirstOpened == false)
                        {
                            selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                            colorDel = true;
                            if (isOpen == false)
                            {
                                anim.SetTrigger("Open");
                                isOpen = true;
                                AudioSource.PlayClipAtPoint(door_open, new Vector3(cam_1.transform.position.x, cam_1.transform.position.y, cam_1.transform.position.z));
                            }
                            else
                            {
                                anim.SetTrigger("Close");
                                isOpen = false;
                                isFirstOpened = true;
                                AudioSource.PlayClipAtPoint(door_close, new Vector3(cam_1.transform.position.x, cam_1.transform.position.y, cam_1.transform.position.z));
                            }
                        }
                        if (isFirstOpened == true)
                        {
                            if (isOpenF == false && trig_help == false)
                            {
                                for (int i = 0; i < 2; i++)
                                {
                                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                                    colorDel = true;
                                    if (isOpen == false)
                                    {
                                        anim.SetTrigger("Open");
                                        isOpen = true;
                                    }
                                    else
                                    {
                                        anim.SetTrigger("Close");
                                        isOpen = false;
                                    }
                                }
                                isOpenF = true;
                                AudioSource.PlayClipAtPoint(door_close, new Vector3(cam_1.transform.position.x, cam_1.transform.position.y, cam_1.transform.position.z));
                            }
                            if (isOpenF == true && trig_help == true)
                            {
                                for (int i = 0; i < 2; i++)
                                {
                                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                                    colorDel = true;
                                    if (isOpen == false)
                                    {
                                        anim.SetTrigger("Open");
                                        isOpen = true;
                                    }
                                    else
                                    {
                                        anim.SetTrigger("Close");
                                        isOpen = false;
                                    }
                                }
                                isOpenF = false;
                                AudioSource.PlayClipAtPoint(door_open, new Vector3(cam_1.transform.position.x, cam_1.transform.position.y, cam_1.transform.position.z));
                            }
                            if (trig_help == false)//антибаг-тригер
                            {
                                trig_help = true;
                            }
                            else
                            {
                                trig_help = false;
                            }
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.R) && assembly.a_step == 7)//переход в режим сборки на двери
                    {
                        if (isOpen == false && isFirstOpened == false)
                        {
                            colorDel = true;
                            assemblyOnDoor = true;
                            pl.transform.position = new Vector3(9.819f, 0.797f, 16.583f);
                            cam_1.transform.rotation = Quaternion.Euler(0, -90, 0);
                        }
                        if (isOpenF == true && isFirstOpened == true)
                        {
                            colorDel = true;
                            assemblyOnDoor = true;
                            pl.transform.position = new Vector3(9.819f, 0.797f, 16.583f);
                            cam_1.transform.rotation = Quaternion.Euler(0, -90, 0);
                        }
                    }
                }
                if (hit.collider.tag != "BoxDoor" && colorDel == false)
                {
                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                    selectedObject = null;
                    colorDel = true;
                }
            }
        }
        if(assemblyOnDoor == true)
        {
            pl.enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            if (Input.GetKeyDown(KeyCode.F))
            {
                pl.transform.position = new Vector3(9.937f, 1.095f, 16.87f);
                cam_1.transform.rotation = Quaternion.Euler(0, -106.6f, 0);
                assemblyOnDoor = false;
                pl.enabled = true;
            }
        }
    }
}

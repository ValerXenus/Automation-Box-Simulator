﻿//    Automation Box Simulator - pause menu script
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using System.Data;
using Mono.Data.Sqlite;

public class PauseScript : MonoBehaviour
{
    public GameObject pause_menu;
    public Transform pl;
    public SqliteConnection conn = SignScript.conn;
    public GameObject save_dialog;
    
    public void button_resume_Click()//Кнопка "Продолжить"
    {
        pause_menu.SetActive(false);
        pl.GetComponent<FirstPersonController>().enabled = true;
    }

    public void button_save_level_Click()//сохранение текущей сборки
    {
        if(SignScript.loaded_level == -1)//если данная сборка начата с 0.
        {
            SqliteCommand cmd2 = new SqliteCommand("INSERT INTO levels (user_id, step_number, points, date) VALUES (@user_id, @step_number, @points, datetime('now','localtime')); " + //date('now') тот же CURDATE()
            "SELECT level_id FROM levels WHERE (user_id = @user_id) AND (step_number = @step_number);", conn);
            cmd2.Parameters.Add(new SqliteParameter("@user_id", SignScript.userID));
            cmd2.Parameters.Add(new SqliteParameter("@step_number", assembly.a_step));
            cmd2.Parameters.Add(new SqliteParameter("@points", Inventory.points));
            SqliteDataReader rd = cmd2.ExecuteReader();
            while(rd.Read())
            {
                SignScript.loaded_level = rd.GetInt32(0);
            }
            rd.Close();
        }
        else//если данная сборка была загружена с сохранения
        {
            SqliteCommand cmd = new SqliteCommand("UPDATE levels SET step_number = @step_number, points = @points, date = datetime('now','localtime') WHERE level_id = @level_id", conn);
            cmd.Parameters.Add(new SqliteParameter("@step_number", assembly.a_step));
            cmd.Parameters.Add(new SqliteParameter("@points", Inventory.points));
            cmd.Parameters.Add(new SqliteParameter("@level_id", SignScript.loaded_level));
            cmd.ExecuteNonQuery();
        }
        save_dialog.SetActive(true);
    }

    public void button_exit_to_main_Click()//выход в главное меню
    {
        SceneManager.LoadScene(0);
        SignScript.loaded_level = -1;
        assembly.a_step = 0;
    }

    public void OnSaveDialogOKClick()
    {
        save_dialog.SetActive(false);
    }

    public void button_quit_Click()//Кнопка выход
    {
        Application.Quit();
    }
	
	void Update ()
    {
        if(pause_menu.gameObject.activeInHierarchy == true)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(pause_menu.gameObject.activeInHierarchy == false)
            {
                pause_menu.SetActive(true);
                pl.GetComponent<FirstPersonController>().enabled = false;
                Cursor.visible = false;
            }
            else
            {
                pause_menu.SetActive(false);
                pl.GetComponent<FirstPersonController>().enabled = true;
            }
        }
	}
}

﻿//    Automation Box Simulator - tablet script
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class TabletScript : MonoBehaviour
{
    private GameObject selectedObject = null;
    private static bool isRead = false;//проверка на наличие режима чтения
    private static int b_page = 0;//номер страницы в методичке
    public GameObject dialog_book;//UI методички
    public GameObject[] pages;//страницы методички
    private bool colorDel = true;//проверка на снятый цвет
    public FirstPersonController pl;
    public AudioSource listanie_met;

    [HideInInspector]
    public bool dist;

    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        if (Physics.Raycast(ray, out hit, 3))
        {
            if (hit.collider.tag == "tb")
            {
                if (selectedObject != null)
                {
                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                    selectedObject = null;
                }
                selectedObject = hit.collider.gameObject;
                selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Selection");//добавить выделение объекта
                dist = true;
                colorDel = false;
                if (Input.GetButtonDown("Use"))//открываем методичку пользователю
                {
                    isRead = true;
                    pl.enabled = false;
                    dialog_book.SetActive(true);
                    switch (b_page)
                    {
                        case 0:
                            pages[0].SetActive(true);
                            break;
                        case 1:
                            pages[1].SetActive(true);
                            break;
                        case 2:
                            pages[2].SetActive(true);
                            break;
                    }
                }
            }
            if (hit.collider.tag != "tb" && colorDel == false)
            {
                selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                selectedObject = null;
                colorDel = true;
            }
        }
    }

    void OnGUI()
    {
        if (colorDel == false && isRead == false)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 250, 30), "Нажмите 'Е' чтобы читать");
        }
        if (isRead == true)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(Screen.width / 2 - 220, Screen.height - 100, 500, 30), "Нажмите 'F' чтобы закрыть. Используйте кнопки мыши, чтобы листать.");
        }
        if (isRead == true)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            if(Input.GetKeyDown(KeyCode.Mouse0))//пролистывание страниц
            {
                listanie_met.Play();
                switch (b_page)
                {
                    case 0:
                        pages[1].SetActive(true);
                        pages[0].SetActive(false);
                        b_page++;
                        break;
                    case 1:
                        pages[2].SetActive(true);
                        pages[1].SetActive(false);
                        b_page++;
                        break;
                    case 2:
                        pages[0].SetActive(true);
                        pages[2].SetActive(false);
                        b_page = 0;
                        break;
                }
            }
            if (Input.GetKeyDown(KeyCode.Mouse1))//пролистывание страниц
            {
                listanie_met.Play();
                switch (b_page)
                {
                    case 0:
                        pages[2].SetActive(true);
                        pages[0].SetActive(false);
                        b_page = 2;
                        break;
                    case 1:
                        pages[0].SetActive(true);
                        pages[1].SetActive(false);
                        b_page--;
                        break;
                    case 2:
                        pages[1].SetActive(true);
                        pages[2].SetActive(false);
                        b_page--;
                        break;
                }
            }
            if (Input.GetKeyDown(KeyCode.F))//выход из режима просмотра
            {
                Cursor.lockState = CursorLockMode.Locked;
                dialog_book.SetActive(false);
                isRead = false;
                pl.enabled = true;
            }
        }
    }
}

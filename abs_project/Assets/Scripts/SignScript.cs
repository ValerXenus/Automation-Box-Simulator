﻿//    Automation Box Simulator - authorization script
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.SceneManagement;
using System.Timers;

public class SignScript : MonoBehaviour
{
    public static string userLog;//статическая пременная Логин пользователя
    public static int userID;//статическая пременная Код пользователя
    public static int loaded_level = -1;//код загруженной сборки
    public GameObject reg_obj;//форма регистрации
    public GameObject log_obj;//форма авторизации
    public GameObject autorization;//Форма для форм регистрации и авторизации.
    public GameObject main_menu;//форма главного меню
    public GameObject dialog_log_conflict; //диалоговое окно существующего логина
    public GameObject dialog_accept_new_acc; //диалоговое окно об успешном создании нового аккаунта
    public GameObject dialog_wrong_log; //диалоговое окно об неправильном логине и пароле
    public GameObject dialog_empty_input;//диалоговое окно, информирующее о пустых полях ввода
    public GameObject intro;//интро при запуске
    public InputField user_Log;//Поле ввода логина
    public InputField user_Pas;//Поле ввода пароля
    private static bool loaded = false;//логическое поле, обозначающее проигранное видео
    public Text user_name;//отображение логина пользователя в главном меню
    public AudioSource background_music;

    public static SqliteConnection conn;

    public void button_signUp_Click()//событие кнопки "Регистрация"
    {
        reg_obj.SetActive(true);
        log_obj.SetActive(false);
    }

    public void button_cancel_Click()//событие кнопки "Отмена"
    {
        reg_obj.SetActive(false);
        log_obj.SetActive(true);
    }

    public void btn_new_assembly_Click()//событие кнопки "Новая сборка"
    {
        SceneManager.LoadScene(1);//загрузка стандартного уровня
        LoadDataScript.isLoadedHistory = false;
        LoadDataScript.isLoadedSaves = false;
    }

    public void btn_exit_Click()//выход в Windows
    {
        Application.Quit();
    }

    public void button_accept_log_conflict()//события кнопки "ОК" на диалоговых окнах
    {
        dialog_log_conflict.SetActive(false);
    }

    public void button_accept_new_acc()
    {
        dialog_accept_new_acc.SetActive(false);
    }

    public void button_accept_wrong_log()
    {
        dialog_wrong_log.SetActive(false);
    }

    public void button_accept_empty()
    {
        dialog_empty_input.SetActive(false);
    }

    static string GetMD5Hash(MD5 md5hash, string input)//функция шифрования пароля по алгоритму MD5
    {
        byte[] data = md5hash.ComputeHash(Encoding.UTF8.GetBytes(input));
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }
        return sBuilder.ToString();
    }

    public void button_log_Click()
    {
        if (user_Log.text != "" && user_Pas.text != "")
        {
            SqliteCommand cmd = new SqliteCommand("SELECT user_id FROM users WHERE (user_log = @user_log) AND (user_pas = @user_pas);", conn);
            cmd.Parameters.Add(new SqliteParameter("@user_log", user_Log.text));
            using (MD5 md5hash = MD5.Create())
            {
                string hash = GetMD5Hash(md5hash, user_Pas.text);
                cmd.Parameters.Add(new SqliteParameter("@user_pas", hash));
            }
            SqliteDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)//если в запросе есть результат
            {
                rd.Read();
                userID = rd.GetInt32(0);
                rd.Close();
                userLog = user_Log.text;
                autorization.SetActive(false);
                main_menu.SetActive(true);
                user_name.text = userLog;
            }
            else
            {
                rd.Close();
                dialog_wrong_log.SetActive(true);
                user_Pas.text = "";
            }
        }
        else
        {
            dialog_empty_input.SetActive(true);
        }
    }

    public void button_reg_Click()//событие кнопки "Зарегистрироваться"
    {
        if (user_Log.text != "" && user_Pas.text != "")
        {
            string log = user_Log.text;
            string pas = user_Pas.text;
            SqliteCommand cmd2 = conn.CreateCommand();
            cmd2.CommandText = "SELECT user_id FROM users WHERE user_log = @user_log";
            cmd2.Parameters.Add(new SqliteParameter("@user_log", log));
            SqliteDataReader rd = cmd2.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Close();
                dialog_log_conflict.SetActive(true);
                user_Pas.text = "";
            }
            else
            {
                rd.Close();
                SqliteCommand dbcmd = new SqliteCommand("INSERT INTO users (user_log, user_pas, user_type) VALUES (@user_log, @user_pas, 1)");
                dbcmd.Parameters.Add(new SqliteParameter("@user_log", log));
                using (MD5 md5hash = MD5.Create())
                {
                    string hash = GetMD5Hash(md5hash, pas);
                    dbcmd.Parameters.Add(new SqliteParameter("@user_pas", hash));
                }
                dbcmd.Connection = conn;
                dbcmd.ExecuteNonQuery();
                reg_obj.SetActive(false);
                log_obj.SetActive(true);
                dialog_accept_new_acc.SetActive(true);
                user_Pas.text = "";
            }
        }
        else
        {
            dialog_empty_input.SetActive(true);
        }
    }

    private void OnTimerEvent(object source, ElapsedEventArgs e)
    {
        loaded = true;
    }

    void Start()
    {
        Application.targetFrameRate = 60;//ограничиваем число кадров в секунду до 60
        string connectionString = "URI=file:abs_db.db";
        conn = new SqliteConnection(connectionString);
        conn.Open();
        if (userLog != null)
        {
            autorization.SetActive(false);
            main_menu.SetActive(true);
        }
        else
        {
            intro.SetActive(true);
            var aTimer = new Timer(9000);
            aTimer.Elapsed += OnTimerEvent;
            aTimer.AutoReset = false;
            aTimer.Enabled = true;
            aTimer.Start();
        }
    }

    void Update()
    {
        if(loaded == true && intro.activeInHierarchy == true)
        {
            loaded = true;
            background_music.Play();
            intro.SetActive(false);
        }
    }
}

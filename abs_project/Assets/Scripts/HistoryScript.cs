﻿//    Automation Box Simulator - this script retrieve history of user assemblies
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Mono.Data.Sqlite;
using UnityEngine.SceneManagement;
using System;
using System.IO;

public class HistoryScript : MonoBehaviour
{
    public Button button;
    public Text nameLabel;
    public Text saveID;
    public SqliteConnection conn = SignScript.conn;
    public static bool isDialog = false;//отображение или скрытие диалога

    public void onExportClick()
    {
        SqliteCommand cmd = new SqliteCommand("SELECT assessment, date FROM history WHERE history_id = @history_id", conn);
        cmd.Parameters.Add(new SqliteParameter("@history_id", Convert.ToInt32(saveID.text)));
        SqliteDataReader rd = cmd.ExecuteReader();
        rd.Read();
        string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Report to Print.html";
        StreamWriter writer = new StreamWriter(link);
        writer.Write("<script>javascript:window.print();</script>");
        writer.Write("<!DOCTYPE HTML>");
        writer.Write("<html>");
        writer.Write("<head>");
        writer.Write("<meta http-equiv = \"Content-Type\" content = \"text/html; charset=UTF-8\"> ");
        writer.Write("<meta name = \"viewport\" content = \"width=device-width, initial-scale=1.0, maximum-scale=1.0\">");
        writer.Write("<title> Произведенная сборка </title>");
        writer.Write("</head>");
        writer.Write("<body>");
        writer.Write("<h1><center>Результат:</center></h1><br>");
        writer.Write("<center><b>Имя пользователя: "+SignScript.userLog+" <br>");
        writer.Write("Количество набранных баллов в результате сборки: "+ Convert.ToString(rd.GetInt32(0))+" <br>");
        writer.Write("Дата сборки: " + Convert.ToString(rd.GetDateTime(1)) + " <br>");
        writer.Write("</b><center/>");
        writer.Write("</body>");
        writer.Write("</html>");
        writer.Close();
        //assembly.a_step = rd.GetInt32(0);
        rd.Close();
        LoadDataScript.isLoadedHistory = false;
        LoadDataScript.isLoadedSaves = false;
        isDialog = true;
    }
}

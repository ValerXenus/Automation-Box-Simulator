﻿//    Automation Box Simulator - show target in center screen
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;

public class target : MonoBehaviour
{
    public Texture2D targetTexture;
    public Rect position;

    void Start()
    {
        position = new Rect((Screen.width - targetTexture.width) / 2, (Screen.height - targetTexture.height) / 2, targetTexture.width, targetTexture.height);
    }

    void OnGUI()
    {
        GUI.DrawTexture(position, targetTexture);
	}
}

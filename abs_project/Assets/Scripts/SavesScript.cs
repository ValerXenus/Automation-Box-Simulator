﻿//    Automation Box Simulator - saves script
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Mono.Data.Sqlite;
using UnityEngine.SceneManagement;
using System;

public class SavesScript : MonoBehaviour
{
    public Button button;
    public Text nameLabel;
    public Text saveID;
    public SqliteConnection conn = SignScript.conn;
    

    public void onSaveClick()
    {
        SqliteCommand cmd = new SqliteCommand("SELECT step_number, points FROM levels WHERE level_id = @level_id", conn);
        cmd.Parameters.Add(new SqliteParameter("@level_id", Convert.ToInt32(saveID.text)));
        SqliteDataReader rd = cmd.ExecuteReader();
        rd.Read();
        assembly.a_step = rd.GetInt32(0);
        Inventory.points = rd.GetInt32(1);
        rd.Close();
        SignScript.loaded_level = Convert.ToInt32(saveID.text);
        SceneManager.LoadScene(1);
        LoadDataScript.isLoadedHistory = false;
        LoadDataScript.isLoadedSaves = false;
    }
}

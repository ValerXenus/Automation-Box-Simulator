﻿//    Automation Box Simulator - retrieve intro when app is starting
//    Copyright(C) 2016  Valery Borisov (Premiere Softworks)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.If not, see<http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class intro : MonoBehaviour
{
    public MovieTexture video;//видеодорожка
    private AudioSource audio_с;//аудиодорожка

    void Start ()
    {
        PlayIntro();
	}

    public void PlayIntro()//проигрывание интро
    {
        GetComponent<RawImage>().texture = video as MovieTexture;
        audio_с = GetComponent<AudioSource>();
        audio_с.clip = video.audioClip;
        video.Play();
        audio_с.Play();
    }
}

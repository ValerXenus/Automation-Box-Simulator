﻿using UnityEngine;
using System.Collections;

public class Inv_item : MonoBehaviour
{
    public int count_v;
    public string item_name;
    public Texture2D texture;
    public GameObject pfb_object;
    public GameObject pfb_assembly_object;
}

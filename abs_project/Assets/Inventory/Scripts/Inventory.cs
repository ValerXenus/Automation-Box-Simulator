﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

public class Inventory : MonoBehaviour
{
    public List<Inv_item> items = new List<Inv_item>();
    public bool show = false;
    public GUISkin skin;
    public FirstPersonController player;
    public bool dist;//проверка, если пользователь попадает в диапазон дистанции до объекта
    private GameObject selectedObject = null;
    private bool colorDel = true;//проверка на снятый цвет
    private bool isDestroyed = false;//проверка на уничтоженный объект
    Transform[] obj_kids;
    public static int points = 500;//счетчик очков

    //предметы на столе
    public GameObject[] it_t;

    //установленные предметы
    public GameObject[] asb;

    //монтажная панель
    public GameObject mounting_plate;

    [HideInInspector]//спрятать свойство в инспекторе
    public static Inv_item curItem;
    public static bool spawned;

	void OnGUI ()
    {
        if (show)
        {
            GUI.skin = skin;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            GUI.Window(0, new Rect(0f, 0f, Screen.width, Screen.height), InventoryBody, "Inventory");
        }
        if (colorDel == false)
        {
            GUI.Label(new Rect(Screen.width / 2 - 125, Screen.height - 100, 250, 30), "Нажмите 'Е' чтобы подобрать предмет");
        }
    }

    void Start()
    {
        Application.targetFrameRate = 60;//органичиваем число кадров в секунду до 60
        if (SignScript.loaded_level > 0)//если пользователь загрузил сохранение
        {
            switch (assembly.a_step)
            {
                case 1:
                    it_t[0].SetActive(false);
                    asb[0].SetActive(true);
                    break;
                case 2:
                    it_t[0].SetActive(false);
                    it_t[1].SetActive(false);
                    asb[0].SetActive(true);
                    asb[1].SetActive(true);
                    break;
                case 3:
                    it_t[0].SetActive(false);
                    it_t[1].SetActive(false);
                    it_t[2].SetActive(false);
                    asb[0].SetActive(true);
                    asb[1].SetActive(true);
                    asb[2].SetActive(true);
                    break;
                case 4:
                    for (int i = 0; i < 4; i++)
                    {
                        it_t[i].SetActive(false);
                        asb[i].SetActive(true);
                    }
                    break;
                case 5:
                    for (int i = 0; i < 5; i++)
                    {
                        it_t[i].SetActive(false);
                        asb[i].SetActive(true);
                    }
                    break;
                case 6:
                    for (int i = 0; i < 6; i++)
                    {
                        it_t[i].SetActive(false);
                        asb[i].SetActive(true);
                    }
                    break;
                case 7:
                    for (int i = 0; i < 7; i++)
                    {
                        it_t[i].SetActive(false);
                        asb[i].SetActive(true);
                    }
                    break;
                case 8:
                    for (int i = 0; i < 8; i++)
                    {
                        it_t[i].SetActive(false);
                        asb[i].SetActive(true);
                    }
                    break;
                case 9:
                    for (int i = 0; i < 8; i++)
                    {
                        it_t[i].SetActive(false);
                        asb[i].SetActive(true);
                    }
                    mounting_plate.transform.position = new Vector3(7.637f, 1.757f,16.583f);
                    mounting_plate.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                    break;
                case 10:
                    for (int i = 0; i < 8; i++)
                    {
                        it_t[i].SetActive(false);
                        asb[i].SetActive(true);
                    }
                    mounting_plate.transform.position = new Vector3(7.637f, 1.757f, 16.583f);
                    mounting_plate.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                    break;
            }
        }
    }

    void InventoryBody(int id)
    {
        if(curItem)
        {
            GUI.color = Color.cyan;
            GUI.Label(new Rect(730f, 60f, 400f, 300f), "Выбранный предмет:");
            GUI.color = Color.white;
            GUI.Label(new Rect(730f, 80f, 400f, 300f), curItem.item_name);
            GUI.color = Color.white;
            GUI.Label(new Rect(730f, 100f, 400f, 300f), "Количество в инвентаре: " + curItem.count_v);
            GUI.Box(new Rect(600f,60f,400f,300f), "");
            if (GUI.Button(new Rect(730f,280f,150f,50f),"Отмена"))
            {
                curItem = null;
            }
        }

        for (int j = 0; j<5; j++)//чертим ячейки инвентаря
        {
            GUILayout.BeginArea(new Rect(60f+105*j, 60f, 600f, 600f));
            for (int i = 0+5*j; i < 5*(j+1); i++)
            {
                if (items[i] != null)
                {
                    if (GUILayout.Button(items[i].texture, GUILayout.Width(100f), GUILayout.Height(100f)))
                    {
                        curItem = items[i];
                        spawned = false;
                    }
                }
                else
                {
                    GUILayout.Box("", GUILayout.Width(100f), GUILayout.Height(100f));
                }
            }
            GUILayout.EndArea();
        }
    }

    void Update()
    {
        if (assembly.isAssemblyRezh == false)
        {
            isDestroyed = false;
            dist = false;
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit, 3))
            {
                if (hit.collider.tag == "Item")
                {
                    if (selectedObject != null)
                    {
                        selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                        selectedObject = null;
                    }
                    selectedObject = hit.collider.gameObject;
                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Selection");//добавить выделение объекта
                    dist = true;
                    colorDel = false;
                    if (Input.GetButtonDown("Use"))//подбор предметов в инвентарь
                    {
                        selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                        Inv_item test = selectedObject.GetComponent<item>().pfb_item_inv;
                        for (int i = 0; i < items.Count; i++)
                        {
                            if (items[i] == null)
                            {
                                items[i] = test;
                                break;
                            }
                        }
                        //
                        if (hit.transform.parent == true && hit.transform.parent.tag != "Empty")//проверка на наличие родителя
                        {
                            Destroy(hit.transform.parent.gameObject);
                        }
                        else
                        {
                            Destroy(hit.transform.gameObject);
                        }
                        colorDel = true;
                        isDestroyed = true;
                    }
                }
                if (hit.collider.tag != "Item" && colorDel == false && isDestroyed == false)
                {
                    selectedObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                    selectedObject = null;
                    colorDel = true;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.I))//открытие и закрытие инвентаря
        {
            show = !show;
            if (assembly.isAssemblyRezh == false && doorScript.assemblyOnDoor == false)
            {
                if (show)
                {
                    player.enabled = false;
                }
                else
                {
                    player.enabled = true;
                }
            }
        }
        if (assembly.isAssemblyRezh == true || doorScript.assemblyOnDoor == true)//Если активен режим сборки
        {
            if (Input.GetKeyDown(KeyCode.T) && curItem != null)//спавн детали
            {
                switch (assembly.a_step)
                {
                    case 0://шаг 1-й установка короба
                        if (curItem.name == "Wirebox_inv")
                        {
                            GameObject[] obj = new GameObject[5];
                            for (int i = 0; i < 5; i++)//спавн разметки коробов
                            {
                                switch (i)
                                {
                                    case 0:
                                        obj[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.204f, 1.064f, 19.211f), Quaternion.identity) as GameObject;
                                        break;
                                    case 1:
                                        obj[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.876f, 1.064f, 19.211f), Quaternion.identity) as GameObject;
                                        break;
                                    case 2:
                                        obj[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.539f, 1.064f, 19.866f), Quaternion.identity) as GameObject;
                                        break;
                                    case 3:
                                        obj[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.539f, 1.064f, 19.569f), Quaternion.identity) as GameObject;
                                        break;
                                    case 4:
                                        obj[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.539f, 1.064f, 19.273f), Quaternion.identity) as GameObject;
                                        break;
                                    case 5:
                                        obj[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.539f, 1.064f, 18.713f), Quaternion.identity) as GameObject;
                                        break;
                                }
                                if (i < 2)
                                {
                                    obj[i].transform.Rotate(new Vector3(-90, 90, 0));
                                    obj[i].transform.localScale = new Vector3(0.6f, 0.8f, 0.6f);
                                }
                                else
                                {
                                    obj[i].transform.Rotate(new Vector3(-90, 0, 0));
                                    obj[i].transform.localScale = new Vector3(0.27f, 0.8f, 0.6f);
                                }
                                obj[i].GetComponent<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                obj[i].GetComponent<Renderer>().material.color = Color.green;
                            }
                        }
                        break;
                    case 1://шаг 2-й установка din-реек
                        if (curItem.name == "Din_rail_inv")
                        {
                            GameObject[] obj_2 = new GameObject[3];
                            for (int i = 0; i < 3; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        obj_2[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.541f, 1.064f, 19.719f), Quaternion.identity) as GameObject;
                                        break;
                                    case 1:
                                        obj_2[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.541f, 1.064f, 19.419f), Quaternion.identity) as GameObject;
                                        break;
                                    case 2:
                                        obj_2[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.541f, 1.064f, 18.598f), Quaternion.identity) as GameObject;
                                        break;
                                }
                                obj_2[i].transform.Rotate(new Vector3(-90, 0, 0));
                                obj_2[i].transform.localScale = new Vector3(0.27f, 0.6f, 0.6f);
                                obj_2[i].GetComponent<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                obj_2[i].GetComponent<Renderer>().material.color = Color.green;
                            }
                        }
                        break;
                    case 2://шаг 3-й установка выключателей
                        GameObject[] obj_3 = new GameObject[3];
                        if (curItem != null)
                        {
                            if (curItem.name == "QF1")
                            {
                                obj_3[0] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.289f, 1.122f, 19.717f), Quaternion.identity) as GameObject;
                                obj_3[0].transform.Rotate(new Vector3(0, 0, -90));
                                obj_3[0].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_3[0].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if(assembly_ex.counter != 0)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "QF2")
                            {
                                obj_3[1] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.356f, 1.122f, 19.717f), Quaternion.identity) as GameObject;
                                obj_3[1].transform.Rotate(new Vector3(0, 0, -90));
                                obj_3[1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_3[1].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 1)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "QF3")
                            {
                                obj_3[2] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.400f, 1.122f, 19.717f), Quaternion.identity) as GameObject;
                                obj_3[2].transform.Rotate(new Vector3(0, 0, -90));
                                obj_3[2].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_3[2].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 2)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        break;
                    case 3: //шаг 4-й установка контроллера и его комплектующих
                        GameObject[] obj_4 = new GameObject[4];
                        if (curItem != null)
                        {
                            if (curItem.name == "G1")
                            {
                                obj_4[0] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.459f, 1.112f, 19.718f), Quaternion.identity) as GameObject;
                                obj_4[0].transform.Rotate(new Vector3(0, 0, -180));
                                obj_4[0].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_4[0].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 0)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "A3_inv")
                            {
                                obj_4[1] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.535f, 1.112f, 19.718f), Quaternion.identity) as GameObject;
                                obj_4[1].transform.Rotate(new Vector3(0, 0, -180));
                                obj_4[1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_4[1].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 1)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "A1_inv")
                            {
                                obj_4[2] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.629f, 1.122f, 19.719f), Quaternion.identity) as GameObject;
                                obj_4[2].transform.Rotate(new Vector3(0, 0, -180));
                                obj_4[2].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_4[2].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 2)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "A2_inv")
                            {
                                obj_4[3] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.728f, 1.112f, 19.719f), Quaternion.identity) as GameObject;
                                obj_4[3].transform.Rotate(new Vector3(0, 0, -180));
                                obj_4[3].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_4[3].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 3)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        break;
                    case 4: //шаг 5-й, размещение компонентов на второй din-рейке
                        GameObject[] obj_5 = new GameObject[6];
                        if (curItem != null)
                        {
                            if (curItem.name == "KM1")
                            {
                                obj_5[0] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.592f, 1.102f, 19.449f), Quaternion.identity) as GameObject;
                                obj_5[0].transform.Rotate(new Vector3(0, -180, -180));
                                obj_5[0].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_5[0].GetComponent<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                obj_5[0].GetComponent<Renderer>().material.color = Color.green;
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 0)
                                {
                                    points -= 50;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "K1-K5")
                            {
                                for (int i = 1; i < 6; i++)
                                {
                                    switch (i)
                                    {
                                        case 1:
                                            obj_5[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.72f, 1.1f, 19.421f), Quaternion.identity) as GameObject;
                                            break;
                                        case 2:
                                            obj_5[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.74f, 1.1f, 19.421f), Quaternion.identity) as GameObject;
                                            break;
                                        case 3:
                                            obj_5[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.76f, 1.1f, 19.421f), Quaternion.identity) as GameObject;
                                            break;
                                        case 4:
                                            obj_5[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.78f, 1.1f, 19.421f), Quaternion.identity) as GameObject;
                                            break;
                                        case 5:
                                            obj_5[i] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.8f, 1.1f, 19.421f), Quaternion.identity) as GameObject;
                                            break;
                                    }
                                    obj_5[i].transform.Rotate(new Vector3(0, 0, -180));
                                    obj_5[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                    obj_kids = obj_5[i].GetComponentsInChildren<Transform>(true);
                                    foreach (Transform gob in obj_kids)
                                    {
                                        gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                        gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                    }
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 1)
                                {
                                    points -= 50;
                                }
                            }
                        }
                        break;
                    case 5: //шаг 6-й, размещение на средней панели
                        GameObject[] obj_6 = new GameObject[2];
                        if (curItem != null)
                        {
                            if(curItem.name == "A5_inv")
                            {
                                obj_6[0] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.545f, 1.081f, 18.897f), Quaternion.identity) as GameObject;
                                obj_6[0].transform.Rotate(new Vector3(0, 0, 180));
                                obj_6[0].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_6[0].GetComponent<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                obj_6[0].GetComponent<Renderer>().material.color = Color.green;
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 0)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "A6_inv")
                            {
                                obj_6[1] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.545f, 1.081f, 19.01f), Quaternion.identity) as GameObject;
                                obj_6[1].transform.Rotate(new Vector3(0, 0, 180));
                                obj_6[1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_6[1].GetComponent<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                obj_6[1].GetComponent<Renderer>().material.color = Color.green;
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 1)
                                {
                                    points -= 25;
                                }
                            }
                        }
                        break;
                    case 6: //шаг 7-й установка клемм на нажней din-рейке
                        GameObject[] obj_7 = new GameObject[5];
                        if (curItem != null)
                        {
                            if (curItem.name == "X1_1 X1_2")
                            {
                                obj_7[0] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.288f, 1.095f, 18.596f), Quaternion.identity) as GameObject;
                                obj_7[1] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.338f, 1.095f, 18.596f), Quaternion.identity) as GameObject;
                                for (int i = 0; i < 2; i++)
                                {
                                    obj_7[i].transform.Rotate(new Vector3(-90, 0, 0));
                                    obj_7[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                    obj_kids = obj_7[i].GetComponentsInChildren<Transform>(true);
                                    foreach (Transform gob in obj_kids)
                                    {
                                        gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                        gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                    }
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 0)
                                {
                                    points -= 10;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "X1_2")
                            {
                                obj_7[2] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.313f, 1.095f, 18.596f), Quaternion.identity) as GameObject;
                                obj_7[3] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.362f, 1.095f, 18.596f), Quaternion.identity) as GameObject;
                                for (int i = 2; i < 4; i++)
                                {
                                    obj_7[i].transform.Rotate(new Vector3(-90, 0, 0));
                                    obj_7[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                    obj_kids = obj_7[i].GetComponentsInChildren<Transform>(true);
                                    foreach (Transform gob in obj_kids)
                                    {
                                        gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                        gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                    }
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 1)
                                {
                                    points -= 10;
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "X3")
                            {
                                obj_7[4] = Instantiate(curItem.pfb_assembly_object, new Vector3(12.751f, 1.095f, 18.596f), Quaternion.identity) as GameObject;
                                obj_7[4].transform.Rotate(new Vector3(-90, 0, 0));
                                obj_7[4].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_7[4].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                                if (assembly_ex.counter != 2)
                                {
                                    points -= 10;
                                }
                            }
                        }
                        break;
                    case 7: //шаг 8-й установка элементов управления на дверце шкафа
                        GameObject[] obj_8 = new GameObject[7];
                        if (curItem != null)
                        {
                            if (curItem.name == "SA1")
                            {
                                obj_8[0] = Instantiate(curItem.pfb_assembly_object, new Vector3(8.125f, 2.32f, 16.435f), Quaternion.identity) as GameObject;
                                obj_8[0].transform.Rotate(new Vector3(0, 90, -90));
                                obj_8[0].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_8[0].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "SA2")
                            {
                                obj_8[1] = Instantiate(curItem.pfb_assembly_object, new Vector3(8.125f, 2.32f, 16.585f), Quaternion.identity) as GameObject;
                                obj_8[1].transform.Rotate(new Vector3(0, 90, -90));
                                obj_8[1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_8[1].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "SA3")
                            {
                                obj_8[2] = Instantiate(curItem.pfb_assembly_object, new Vector3(8.125f, 1.966f, 16.435f), Quaternion.identity) as GameObject;
                                obj_8[2].transform.Rotate(new Vector3(0, 90, -90));
                                obj_8[2].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_8[2].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "SA4")
                            {
                                obj_8[3] = Instantiate(curItem.pfb_assembly_object, new Vector3(8.125f, 1.966f, 16.585f), Quaternion.identity) as GameObject;
                                obj_8[3].transform.Rotate(new Vector3(0, 90, -90));
                                obj_8[3].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_8[3].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "HA1")
                            {
                                obj_8[4] = Instantiate(curItem.pfb_assembly_object, new Vector3(8.125f, 2.32f, 16.735f), Quaternion.identity) as GameObject;
                                obj_8[4].transform.Rotate(new Vector3(0, 90, -90));
                                obj_8[4].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_8[4].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "SB1")
                            {
                                obj_8[5] = Instantiate(curItem.pfb_assembly_object, new Vector3(8.16f, 1.965f, 16.735f), Quaternion.identity) as GameObject;
                                obj_8[5].transform.Rotate(new Vector3(0, 90, -90));
                                obj_8[5].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_kids = obj_8[5].GetComponentsInChildren<Transform>(true);
                                foreach (Transform gob in obj_kids)
                                {
                                    gob.GetComponentInChildren<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                    gob.GetComponentInChildren<Renderer>().material.color = Color.green;
                                }
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                            }
                        }
                        if (curItem != null)
                        {
                            if (curItem.name == "A4_inv")
                            {
                                obj_8[6] = Instantiate(curItem.pfb_assembly_object, new Vector3(8.116f, 2.093f, 16.585f), Quaternion.identity) as GameObject;
                                obj_8[6].transform.Rotate(new Vector3(270, 90, 0));
                                obj_8[6].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                obj_8[6].GetComponent<Renderer>().material.shader = Shader.Find("Unlit/XRay");
                                obj_8[6].GetComponent<Renderer>().material.color = Color.green;
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (items[i] == curItem)
                                    {
                                        items[i] = null;
                                        curItem = null;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    //шаг 9-й установка монтажной панели
                }
            }
            if (assembly.a_step == 1 && curItem != null)
            {
                if (curItem.name == "Wirebox_inv")
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i] == curItem)
                        {
                            items[i] = null;
                            curItem = null;
                            break;
                        }
                    }
                }
            }
            if (assembly.a_step == 2 && curItem != null)
            {
                if (curItem.name == "Din_rail_inv")
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i] == curItem)
                        {
                            items[i] = null;
                            curItem = null;
                            break;
                        }
                    }
                }
            }

        }
    }
}
